package com.ember.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.ember.jpa.entities.News;
import com.ember.jpa.entities.NewsTypeSubscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ember.jpa.repository.NewsRepository;
import com.ember.jpa.repository.NewsTypeSubscriptionRepository;
import com.ember.jpa.entities.NewsType;
import com.ember.service.NewsService;
import com.ember.service.exception.RuntimeSystemException;
import com.ember.service.exception.SystemException;
import com.ember.service.exception.SystemException.ErrorCode;
import org.springframework.util.StringUtils;

@Service
public class DefaultNewsService implements NewsService {

	@Autowired private NewsRepository newsRepository;

	@Transactional
	public void add(News news) {
		if(StringUtils.isEmpty(news.getName()) || (news.getNewsType() == null &&
				StringUtils.isEmpty(news.getNewsType().getName()))) {
			throw new RuntimeSystemException(new SystemException("All fields are required!", ErrorCode.NULL_PROPERTY));
		}

		newsRepository.save(news);
	}

	@Override
	public Page<News> findAll(Pageable pageRequest) {
		return newsRepository.findAll(pageRequest);
	}

}
