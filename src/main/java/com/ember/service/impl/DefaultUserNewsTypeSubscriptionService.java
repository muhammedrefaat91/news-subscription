package com.ember.service.impl;

import com.ember.jpa.entities.News;
import com.ember.jpa.entities.NewsType;
import com.ember.jpa.entities.NewsTypeSubscription;
import com.ember.jpa.repository.NewsRepository;
import com.ember.jpa.repository.NewsTypeSubscriptionRepository;
import com.ember.service.NewsService;
import com.ember.service.UserNewsTypeSubscriptionService;
import com.ember.service.exception.RuntimeSystemException;
import com.ember.service.exception.SystemException;
import com.ember.service.exception.SystemException.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class DefaultUserNewsTypeSubscriptionService implements UserNewsTypeSubscriptionService {

	@Autowired private NewsRepository newsRepository;
	@Autowired private NewsTypeSubscriptionRepository newsTypeSubscriptionRepository;


	@Override
	public Page<News> findAllNewsTypeSubscribedByUserId(int userId,  Pageable pageRequest) {
		if (userId <= 0) {
			throw new RuntimeSystemException(new SystemException("user id doesn't exist!", ErrorCode.NOT_FOUND));
		}

		Page<News> newsPage = newsRepository.findAllSubscriptionNewsTypesByUserId(userId, pageRequest);

		return newsPage;
	}


	@Transactional
	public void unSubscribe(int userId, int newsTypeId) {
		if(userId <= 0 || (newsTypeId <= 0)) {
			throw new RuntimeSystemException(new SystemException("All fields are required!", ErrorCode.NULL_PROPERTY));
		}

		newsTypeSubscriptionRepository.deleteByUserIdAndNewsTypeId(userId, newsTypeId);
	}


	@Transactional
	public void subscribe(int userId, int newsTypeId) {
		if(userId <= 0 || (newsTypeId <= 0)) {
			throw new RuntimeSystemException(new SystemException("All fields are required!", ErrorCode.NULL_PROPERTY));
		}

		NewsTypeSubscription newsTypeSubscription = newsTypeSubscriptionRepository.findByUserIdAndNewsTypeId(userId, newsTypeId);

		if (newsTypeSubscription != null) {
			throw new RuntimeSystemException(new SystemException("User already has subscribed with this news type", ErrorCode.NOT_UNIQUE));
		}
		newsTypeSubscriptionRepository.save(new NewsTypeSubscription(userId, newsTypeId));
	}

}
