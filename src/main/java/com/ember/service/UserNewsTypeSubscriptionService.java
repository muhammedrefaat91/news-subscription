package com.ember.service;

import com.ember.jpa.entities.News;
import com.ember.jpa.entities.NewsType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


/**
 * @author mohamed.refaat
 *
 */
public interface UserNewsTypeSubscriptionService {
	/** User news subscribe
	 * @param userId
	 */
	void subscribe(int userId, int newsTypeId);

	/** User news unsubscribe
	 * @param userId
	 */
	void unSubscribe(int userId, int newsTypeId);


	/** find all news type subscribed by a user
	 * @return { Page}
	 */
	Page<News> findAllNewsTypeSubscribedByUserId(int userId,  Pageable pageRequest);

}
