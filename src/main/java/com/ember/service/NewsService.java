package com.ember.service;

import com.ember.jpa.entities.News;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * @author mohamed.refaat
 *
 */
public interface NewsService {

	/** add new news
	 * @param news
	 */
	void add(News news);


	/** find all news type
	 * @return { Page}
	 */
	Page<News> findAll(Pageable pageRequest);

}
