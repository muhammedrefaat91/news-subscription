package com.ember.rest;

import com.ember.common.CommonUtils;
import com.ember.jpa.entities.News;
import com.ember.jpa.entities.NewsType;
import com.ember.service.NewsService;
import com.ember.service.UserNewsTypeSubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/news")
@RestController
public class NewsController {

	@Autowired
	private NewsService newsService;

	@Autowired
	private UserNewsTypeSubscriptionService userNewsTypeSubscriptionService;

	@GetMapping
	public ResponseEntity<List<News>> getAllNewsType(
 			@PageableDefault(size = 20) Pageable pageRequest) {

		Page<News> newsPage = newsService.findAll(pageRequest);
		return new ResponseEntity(newsPage, HttpStatus.OK);
	}

	@GetMapping("/{userId}")
	public ResponseEntity<List<News>> getAllUserNewsTypeSubscription(@PathVariable(name = "userId") int userId,
			@PageableDefault(size = 20) Pageable pageRequest) {

		Page<News> newsPage = userNewsTypeSubscriptionService.findAllNewsTypeSubscribedByUserId(userId, pageRequest);
		return new ResponseEntity(newsPage, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<Object> addNews(@RequestBody() News news) {
		try {
			newsService.add(news);
			return new ResponseEntity<Object>(HttpStatus.CREATED);
		} catch (Exception ex) {
			throw CommonUtils.handleException(ex);
		}
	}

	@PostMapping("/subscribe/{userId}/{newsTypeId}")
	public ResponseEntity<Object> userNewsTypeSubscribe(@PathVariable int userId, @PathVariable("newsTypeId") int newsTypeId) {
		try {
			userNewsTypeSubscriptionService.subscribe(userId, newsTypeId);
			return new ResponseEntity<Object>(HttpStatus.CREATED);
		} catch (Exception ex) {
			throw CommonUtils.handleException(ex);
		}
	}

	@PostMapping("/unsubscribe/{userId}/{newsTypeId}")
	public ResponseEntity<Object> userNewsTypeUnSubscribe(@PathVariable int userId, @PathVariable("newsTypeId") int newsTypeId) {
		try {
			userNewsTypeSubscriptionService.unSubscribe(userId, newsTypeId);
			return new ResponseEntity<Object>(HttpStatus.CREATED);
		} catch (Exception ex) {
			throw CommonUtils.handleException(ex);
		}
	}

}
