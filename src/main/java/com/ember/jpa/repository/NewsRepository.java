package com.ember.jpa.repository;

import com.ember.jpa.entities.News;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsRepository extends JpaRepository<News, Integer> {

    @Query("select n from News n inner join NewsTypeSubscription newsSubscription " +
            " on n.newsType.id = newsSubscription.newsTypeId where newsSubscription.userId = ?1 ")
    Page<News> findAllSubscriptionNewsTypesByUserId(int userId,  Pageable pageRequest);
}
