package com.ember.jpa.repository;

import com.ember.jpa.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	/** find user by email
	 * @param email
	 * @return  User
	 */
	Optional<User> findByEmail(String email);

	
}
