package com.ember.jpa.repository;


import com.ember.jpa.entities.NewsTypeSubscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsTypeSubscriptionRepository extends JpaRepository<NewsTypeSubscription, Integer> {

    void deleteByUserIdAndNewsTypeId(int userId, int newsTypeId);

    /**
     * find if user already subscribed with given news type Id
     */
    NewsTypeSubscription findByUserIdAndNewsTypeId(int userId, int newsTypeId);
}
