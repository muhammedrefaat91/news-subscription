package com.ember.jpa.entities;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Table(name = "news_type_subscription", uniqueConstraints = { @UniqueConstraint(columnNames = { "user_id", "news_type_id" }) })
@Entity
public class NewsTypeSubscription implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "user_id", nullable = false)
	private Integer userId;

	@Column(name = "news_type_id", nullable = false)
	private Integer newsTypeId;

//	@OneToMany()
//	private List<News> bodies;


	public NewsTypeSubscription() {
	}

	public NewsTypeSubscription(Integer userId, Integer newsTypeId) {
		this.userId = userId;
		this.newsTypeId = newsTypeId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getNewsTypeId() {
		return newsTypeId;
	}

	public void setNewsTypeId(Integer newsTypeId) {
		this.newsTypeId = newsTypeId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		NewsTypeSubscription that = (NewsTypeSubscription) o;
		return id == that.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "NewsTypeSubscription{" +
				"id=" + id +
				", userId=" + userId +
				", newsTypeId=" + newsTypeId +
				'}';
	}
}
